import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-pagina',
  templateUrl: './no-pagina.component.html',
  styleUrls: ['./no-pagina.component.css']
})
export class NoPaginaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
